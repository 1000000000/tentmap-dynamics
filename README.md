# Tentmap Dynamics

A small program to produce gifs comparing the evolution of the double map as compared to the tent map on complex numbers.

### Usage

The program runs from the command line with the following options

    tentmap-dynamics [-?|--help] [-w|--width WIDTH] [-h|--height HEIGHT]
                            [-o|--output OUTPUT] [--dither] [--looping-off]
                            [--loop-repeat ARG] [--box-radius RADIUS]
                            [--resolution NUM] [--frame-length CENT] [--frames NUM]
      Command-line diagram generation.
    
    Available options:
      -?,--help                Show this help text
      -w,--width WIDTH         Desired WIDTH of the output image
      -h,--height HEIGHT       Desired HEIGHT of the output image
      -o,--output OUTPUT       OUTPUT file
      --dither                 Turn on dithering.
      --looping-off            Turn looping off
      --loop-repeat ARG        Number of times to repeat
      --box-radius RADIUS      radius of the box of sample points (default: 2.0)
      --resolution NUM         number of gaps between sample points in each
                               row/column (default: 10)
      --frame-length CENT      length of each frame of the gif in
                               centiseconds (default: 100)
      --frames NUM             number of frames of the gif, with each frame applying
                               the deformation again (default: 10)

### Double Map vs. Tent Map

For a complex number $`z = re^{i\theta}`$, the double map performs the operation $`z \mapsto z^2`$,
while the tent map (or at least my version of it) performs the operation $`z \mapsto z^{T(\theta)}`$,
where

```math
T(\theta) = \begin{cases}
	2 & \theta \leq \pi \\
	\tfrac{2(1 - \theta)}{\theta} & \theta \geq \pi
\end{cases}
```

While the double map only has fixed points at the origin and at $`1`$, the tent map has fixed points for all $`z = re^{\tfrac{4\pi}{3}i}`$.
Furthermore points on rays extending at some angles that are rational multiples of $`\pi`$. 

Their differences over 9 steps as shown by the output of the program (double map is on left, tent map is on right):

![double map and tent map](img/doublemap-tentmap.gif "double map and tent map")
