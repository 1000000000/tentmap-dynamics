{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeFamilies              #-}

module Main where

import Codec.Picture.Gif (GifDelay)

import Control.Arrow hiding ((|||))

import Control.Lens hiding ((#), none)

import Diagrams.Prelude hiding ((<>),option,value)
import Diagrams.TwoD.Types
import Diagrams.Backend.Cairo.CmdLine
import Diagrams.Backend.CmdLine hiding (output)

import Numeric.Lens

import Options.Applicative

points :: Double -> Integer -> [(Point V2 Double, Diagram B)]
points boxRad resolution = do
  let
    sep :: Double
    sep = 2 * boxRad / fromInteger resolution
  x <- [negate boxRad, sep-boxRad .. boxRad]
  y <- [negate boxRad, sep-boxRad .. boxRad]
  let
    pointColor :: Colour Double
    pointColor = sRGB ((x + boxRad) / (boxRad*2)) ((y + boxRad) / (boxRad*2)) 0.5
  return ( p2 (x,y) , circle (sep/4) # fc pointColor # lw (local (sep/20) `atMost` output 0.5) )

p2PolarIso :: RealFloat n => Iso' (P2 n) (n, Angle n)
p2PolarIso = p2Iso . from r2Iso . r2PolarIso

fracPart :: RealFrac a => a -> a
fracPart x = x - fromIntegral (floor x)

tentMap :: Deformation V2 V2 Double
tentMap = Deformation $ \p ->
  let
    (r, theta) = p ^. p2PolarIso
    thetaTurn = fracPart $ theta ^. turn
    tent
      | thetaTurn <= 0.5 = 2
      | thetaTurn >= 0.5 = 2*(1 - thetaTurn) / thetaTurn
      | otherwise = error $ show thetaTurn
  in
    (r**tent , tent*thetaTurn @@ turn) ^. from p2PolarIso

doubleMap :: Deformation V2 V2 Double
doubleMap = Deformation $ \p ->
  let
    (r, theta) = p ^. p2PolarIso
    thetaTurn = theta ^. turn
  in
    (r ^ 2 , 2*thetaTurn @@ turn) ^. from p2PolarIso

newtype Dynamical a = Dynamical a

data DynamicalOpts = DynamicalOpts
  { _boxRad      :: Double
  , _resolution  :: Integer
  , _frameLength :: Integer
  , _numFrames   :: Integer
  }
makeLenses ''DynamicalOpts

instance Parseable DynamicalOpts where
  parser =
    DynamicalOpts
      <$> option auto
         ( long "box-radius"
        <> metavar "RADIUS"
        <> help "radius of the box of sample points"
        <> showDefault
        <> value 2
         )
      <*> option auto
         ( long "resolution"
        <> metavar "NUM"
        <> help "number of gaps between sample points in each row/column"
        <> showDefault
        <> value 20
         )
      <*> option auto
         ( long "frame-length"
        <> metavar "CENT"
        <> help "length of each frame of the gif in centiseconds"
        <> showDefault
        <> value 100
         )
      <*> option auto
        ( long "frames"
       <> metavar "NUM"
       <> help "number of frames of the gif, with each frame applying the deformation again"
       <> showDefault
       <> value 10
        )

type GifAnimation = [(QDiagram Cairo V2 Double Any, GifDelay)]
type DynamicalAnimation = Double -> Integer -> [QDiagram Cairo V2 Double Any]

instance Mainable (Dynamical DynamicalAnimation) where
  type MainOpts (Dynamical DynamicalAnimation) = (MainOpts GifAnimation, DynamicalOpts)

  mainRender (opts, dynamicalOpts) (Dynamical d)
    = (mainRender :: MainOpts GifAnimation -> GifAnimation -> IO ()) opts
      $ take (fromIntegral $ dynamicalOpts ^. numFrames)
        $ zip
          (d (dynamicalOpts ^. boxRad) (dynamicalOpts ^. resolution))
          (repeat $ fromIntegral $ dynamicalOpts ^. frameLength)

renderDynamical :: Double -> Integer -> [Diagram Cairo]
renderDynamical boxRad resolution =
  zipWith mergeDiagrams (buildDynamics doubleMap) (buildDynamics tentMap)
  where
    buildDynamics deformation
      = (<> axes)
      . withEnvelope (envelope :: D V2 Double)
      . clipBy envelope . position
      <$> iterate (fmap $ first $ deform deformation) (points boxRad resolution)
    axes = (circle 1 <> hrule (3*boxRad) <> vrule (3*boxRad)) # lw ultraThin # lc gray
    envelope :: (InSpace V2 Double t, TrailLike t) => t
    envelope = square (4*boxRad)
    mergeDiagrams left right = left ||| vrule (4*boxRad) # lc gray ||| right

main :: IO ()
main = mainWith $ Dynamical $ renderDynamical
